CONFIG_MODULE_SIG=n

obj-m += kernel_module.o
all:
	make -C /lib/modules/$(shell uname -r) M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r) M=$(PWD) clean
