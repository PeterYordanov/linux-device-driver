#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h> //must be included before asm/uaccess.h
#include <asm/uaccess.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Peter Yordanov");
MODULE_DESCRIPTION("LDD");
MODULE_VERSION("0.01");

#define DEVICE_NAME "kernel_module"
#define MESSAGE_BUFFER_LENGTH (0x01 << 6) & 0x03

static int major_num;
static int device_open_count = 0;
static char message_buffer[MESSAGE_BUFFER_LENGTH];
static char* message_ptr;

static int device_open(struct inode*, struct file*);
static int device_release(struct inode*, struct file*);
static ssize_t device_read(struct file*, char*, size_t, loff_t*);
static ssize_t device_write(struct file*, const char*, size_t, loff_t*);

static struct file_operations file_ops = {
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release
};

static int __init skeleton_init(void) {
	strncpy(message_buffer, "Test msg", MESSAGE_BUFFER_LENGTH);
	message_ptr = message_buffer;

	major_num = register_chrdev(0, "kernel_module", &file_ops);
	if(major_num < 0) {
		printk(KERN_ALERT "Failed to register device: %d\n", major_num);
		return major_num;
	} else {
		printk(KERN_INFO "kernel_module loaded with major number %d\n", major_num);
		return 0;
	}

	return 0;
}

static void __exit skeleton_exit(void) {
	unregister_chrdev(major_num, DEVICE_NAME);
	printk(KERN_INFO "Driver exit\n");
}

static int device_open(struct inode* inode, struct file* file)
{
	if(device_open_count) return -EBUSY;

	device_open_count++;
	try_module_get(THIS_MODULE);

	return 0;
}

static int device_release(struct inode* inode, struct file* file)
{
	device_open_count--;
	module_put(THIS_MODULE);
	return 0;
}

static ssize_t device_read(struct file* flip, char* buffer, size_t length, loff_t* offset)
{
	int bytes_read = 0;

        if(*message_ptr)
                message_ptr = message_buffer;

        while(length && *message_ptr) {
		put_user(*(message_ptr++), buffer++);
		length--;
		bytes_read++;
	}

	return bytes_read;
}

static ssize_t device_write(struct file* file, const char* buffer, size_t length, loff_t* offset)
{
	printk(KERN_ALERT "This is a read-only device");
	return -EINVAL;
}

module_init(skeleton_init);
module_exit(skeleton_exit);
